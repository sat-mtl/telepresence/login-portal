import React from 'react'

/**
 * Renders the header of the LoginPortal application
 * @selector `#LoginHeader`
 * @param {string} title - The title of the header
 * @returns {external:react/Component} The welcome hub header
 */
const LoginHeader = () => {
  return (
    <header>
      <div className='Header'>
        <h3>Scenic Light</h3>
      </div>
    </header>
  )
}

export default LoginHeader

import React, { useContext } from 'react'
import { Context } from '@sat-valorisation/ui-components'

import LoginHeader from '@components/LoginHeader'
import LoginForm from '@components/LoginForm'

import '@styles/App.scss'

const { ThemeContext } = Context

/**
 * Renders the layout of the login portal page
 * @selector `#PageLayout`
 * @returns {external:react/Component} The page layout
 */
const PageLayout = () => {
  const theme = useContext(ThemeContext)
  return (
    <div id='PageLayout' className={`Page-${theme}`}>
      <LoginHeader />
      <main>
        <LoginForm />
      </main>
    </div>
  )
}

export default PageLayout

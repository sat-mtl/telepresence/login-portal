import React from 'react'

import PageLayout from './PageLayout'

// @todo Debug ui-components css, it triggers the heap error
import '@sat-valorisation/ui-components/ui-components.css'
import { Context } from '@sat-valorisation/ui-components'

import '@styles/App.scss'

const { ThemeProvider } = Context

/**
* Renders the App
*/
export default function App () {
  return (
    <ThemeProvider value='gabrielle'>
      <PageLayout />
    </ThemeProvider>
  )
}
